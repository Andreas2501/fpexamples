package topten;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class TopTenJava {

	public TopTenJava() {
		String s = "one three three three four four four two two four eight eight six six six six six six seven seven seven seven seven seven five five five five five seven eight eight eight eight eight eight nine nine nine nine nine nine nine nine nine ten ten ten ten ten ten ten ten ten ten eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven";
			
		 Map<String, List<String>> groupByIdentity = 
				 Arrays.asList(s.split(" ")).stream().collect(Collectors.groupingBy(identity -> identity));
	
		 groupByIdentity				 .
				 entrySet().stream().map((Entry<String, List<String>>e ) ->  
					 new Tuple<String,Integer>(e.getKey(), e.getValue().size())).sorted((t1,t2) -> Integer.compare(t1.y,t2.y) * -1).
					 forEach(t -> System.out.println(t.x+" "+t.y));		
	}
	
	public class Tuple<X, Y> { 
		  public final X x; 
		  public final Y y; 
		  
		  public Tuple(X x, Y y) { 
		    this.x = x; 
		    this.y = y; 
		  } 
		} 
	
	public static void main(String[] args) {
		new TopTenJava();
	}
	
}
