-- | Main entry point to the application.
-- | https://www.fpcomplete.com/page/project-build
import Control.Monad
import Data.Ord
import Data.Monoid
import Data.List

-- | The main entry point.
main :: IO ()

s = "one three three three four four four two two four eight eight six six six six six six seven seven seven seven seven seven five five five five five seven eight eight eight eight eight eight nine nine nine nine nine nine nine nine nine ten ten ten ten ten ten ten ten ten ten eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven"

infixl 1 |>

x |> f = f x

main = words s
    |> group . sort
    |> map (\str -> (str, length str))
    |> sortBy (flip (comparing snd) <> comparing fst)
    |> mapM_ print

main' = mapM_ print
      . sortBy (flip (comparing snd) <> comparing fst)
      . map (\str -> (str, length str))
      . group . sort
      . words
      $ s