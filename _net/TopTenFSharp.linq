<Query Kind="FSharpProgram">
  <Reference>&lt;RuntimeDirectory&gt;\System.Drawing.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\WPF\WindowsBase.dll</Reference>
</Query>

let s1 = "one three three three four four four two two four eight eight six six six six six six seven seven seven seven seven seven five five five five five seven eight eight eight eight eight eight nine nine nine nine nine nine nine nine nine ten ten ten ten ten ten ten ten ten ten eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven"

let res = s1.Split([|' '|]) 
       |>  Seq.groupBy (fun x -> x) 
	   |>  Seq.map (fun (a,b) -> (a,(Seq.toList b).Length)) 
       |>  Seq.sortBy(fun (a,b) -> -b) 

res.Dump()