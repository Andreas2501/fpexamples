(ns topten)
 (defn assoc-inc [amap key] (assoc amap key (inc (get amap key 0))))
 
 (defn split-ws [s] (clojure.string/split s #"\s+"))
 
 (def input "one three three three four four four two two four eight eight six six six six six six seven seven seven seven seven seven five five five five five seven eight eight eight eight eight eight nine nine nine nine nine nine nine nine nine ten ten ten ten ten ten ten ten ten ten eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven eleven")
 
 ;;(load-file "../../Users/andreas.scheinert/Documents/GitHub/fpexamples/jvm/TopTen.clj")
 ;;(ns topten)
 ;;(sort-by val > (reduce assoc-inc {} (split-ws input)))
 