-module(assoc).
-export([assoc/2]).

assoc(M, K) ->
    case maps:is_key(K, M) of
        true ->                                                           %The key is there
            maps:put(K, maps:get(K, M) + 1, M);
        false ->                                                          %No key
            maps:put(K, 1, M)
    end.