  import scala.collection.mutable.Map
   
  def countWords(text: Array[String]) = {
          val counts = Map.empty[String, Int]
          for (word <- text) {
            val oldCount =
              if (counts.contains(word))
                  counts(word)
              else
               0
            counts += (word -> (oldCount + 1))          }
          counts
    }
	
	def countWords(text: Array[String]):Map[String,Int] = {  
     val getValueOrZero = (m:Map[String,Int],key:String) => {val r=m.get(key); if(r.isDefined) r.get else 0 }
  
     text.foldLeft(Map.empty[String, Int]){ case(m,w)=> m + (w -> (getValueOrZero(m,w) + 1))}
    }
		
		